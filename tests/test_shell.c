/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <g3k.h>
#include <glib.h>
#include <xrd.h>

static int
_test_shell ()
{
  XrdShell *shell = xrd_shell_new ();
  g_assert_nonnull (shell);

  G3kContext    *g3k = xrd_shell_get_g3k (shell);
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);

  GulkanTexture *texture
    = gulkan_texture_new_from_resource (gulkan, "/res/cursor.png",
                                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                        TRUE);

  VkExtent2D extent = gulkan_texture_get_extent (texture);

  graphene_size_t size_meters = {(float) extent.width / 0.25f,
                                 (float) extent.height / 0.25f};

  XrdWindow *window = xrd_window_new (xrd_shell_get_g3k (shell), "win.", NULL,
                                      extent, &size_meters);

  g_print ("calling g3k_plane_set_texture\n");
  g3k_plane_set_texture (G3K_PLANE (window), texture);

  xrd_shell_add_window (shell, window, NULL, TRUE, NULL);

  GulkanDevice *device = gulkan_context_get_device (gulkan);
  gulkan_device_wait_idle (device);

  g3k_context_render (xrd_shell_get_g3k (shell));
  gulkan_device_wait_idle (device);

  g3k_context_render (xrd_shell_get_g3k (shell));
  gulkan_device_wait_idle (device);

  // TODO: Ref window in shell
  // g_object_unref (window);

  g_object_unref (shell);

  return EXIT_SUCCESS;
}

int
main ()
{
  return _test_shell ();
}
