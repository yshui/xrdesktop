/*
 * xrdesktop
 * Copyright 2019-2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "xrd-shell.h"

#include <gdk/gdk.h>

#include "xrd-version.h"

#define WINDOW_MIN_DIST .05f
#define WINDOW_MAX_DIST 15.f

#define APP_NAME "xrdesktop"

enum
{
  CLICK_EVENT,
  MOVE_CURSOR_EVENT,
  KEYBOARD_PRESS_EVENT,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0};

enum
{
  WM_ACTIONSET,
  SYNTH_ACTIONSET,
  LAST_ACTIONSET
};

struct _XrdShell
{
  GObject parent;

  XrdInputSynth *input_synth;

  G3kKeyboard *keyboard;

  G3kButton *button_reset;
  G3kButton *button_sphere;
  G3kButton *button_pinned_only;
  G3kButton *button_selection_mode;
  G3kButton *button_ignore_input;

  gboolean pinned_only;
  gboolean selection_mode;
  gboolean ignore_input;
  // ignore_input: input will only be handled for buttons, not windows

  XrdWindow *keyboard_window;

  gulong keyboard_press_signal;
  gulong keyboard_close_signal;

  double analog_threshold;

  double scroll_to_push_ratio;
  double scroll_to_scale_ratio;

  G3kCursor *cursor;

  G3kContainer *wm_control_container;

  /* maps a key to desktop #XrdWindows, but not buttons. */
  GHashTable *window_mapping;

  GxrActionSet *action_sets[LAST_ACTIONSET];
  GxrManifest  *manifests[LAST_ACTIONSET];

  G3kContext *g3k;

  G3kBackground *background;

  G3kObject *windows_node;

  GxrAction *haptic_action;
};

G_DEFINE_TYPE (XrdShell, xrd_shell, G_TYPE_OBJECT)

static void
_finalize (GObject *gobject);

static gboolean
_init_buttons (XrdShell *self, GxrController *controller);

static void
_destroy_buttons (XrdShell *self);

static void
_add_window_callbacks (XrdShell *self, XrdWindow *window);

static void
_add_button_callbacks (XrdShell *self, G3kButton *button);

static void
_keyboard_press_cb (G3kKeyboard *keyboard,
                    G3kKeyEvent *event,
                    gpointer     user_data);

static void
_keyboard_pre_show_cb (G3kKeyboard *keyboard, gpointer user_data);

static void
_keyboard_post_hide_cb (G3kKeyboard *keyboard, gpointer user_data);

static void
_init_keyboard (XrdShell *self)
{
  G3kContext *context = xrd_shell_get_g3k (self);
  self->keyboard = g3k_keyboard_new (context);

  GHashTable *modes = g3k_keyboard_get_modes (self->keyboard);
  g_autoptr (GList) containers = g_hash_table_get_values (modes);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);

  for (GList *container = containers; container != NULL;
       container = container->next)
    {
      g3k_object_manager_add_container (manager, container->data);

      GSList            *buttons = g3k_container_get_children (container->data);
      graphene_point3d_t dummy = {.x = 0.0f, .y = 0.0f, .z = 0.0f};

      for (GSList *button = buttons; button != NULL; button = button->next)
        {
          xrd_shell_add_button (self, G3K_BUTTON (button->data), &dummy,
                                G_CALLBACK (g3k_keyboard_click_cb),
                                self->keyboard, NULL);
        }
      g_slist_free (buttons);
    }

  g_signal_connect (self->keyboard, "key-press-event",
                    G_CALLBACK (_keyboard_press_cb), self);
  g_signal_connect (self->keyboard, "pre-show-event",
                    G_CALLBACK (_keyboard_pre_show_cb), self);
  g_signal_connect (self->keyboard, "post-hide-event",
                    G_CALLBACK (_keyboard_post_hide_cb), self);
}

static void
xrd_shell_class_init (XrdShellClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = _finalize;

  signals[CLICK_EVENT] = g_signal_new ("click-event", G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                       G_TYPE_NONE, 1,
                                       GDK_TYPE_EVENT
                                         | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[MOVE_CURSOR_EVENT] = g_signal_new ("move-cursor-event",
                                             G_TYPE_FROM_CLASS (klass),
                                             G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                             NULL, G_TYPE_NONE, 1,
                                             GDK_TYPE_EVENT
                                               | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[KEYBOARD_PRESS_EVENT] = g_signal_new ("keyboard-press-event",
                                                G_TYPE_FROM_CLASS (klass),
                                                G_SIGNAL_RUN_LAST, 0, NULL,
                                                NULL, NULL, G_TYPE_NONE, 1,
                                                G_TYPE_POINTER
                                                  | G_SIGNAL_TYPE_STATIC_SCOPE);
}

XrdShell *
xrd_shell_new_from_vulkan_extensions (GSList *instance_ext_list,
                                      GSList *device_ext_list)
{
  G3kContext *g3k = g3k_context_new_full (instance_ext_list, device_ext_list,
                                          APP_NAME, XRD_VERSION_HEX);
  if (!g3k)
    {
      g_printerr ("Could not init VR runtime.\n");
      return NULL;
    }

  return xrd_shell_new_from_g3k (g3k);
}

XrdShell *
xrd_shell_new (void)
{
  return xrd_shell_new_from_vulkan_extensions (NULL, NULL);
}

/**
 * xrd_shell_add_window:
 * @self: The #XrdShell
 * @window: The #XrdWindow to add
 * @parent: The parent node in the scene graph. If NULL, the root node is used.
 * @draggable: Desktop windows should set this to TRUE. This will enable the
 * expected interaction of being able to grab windows and drag them around.
 * It should be set to FALSE for example for
 *  - child windows
 *  - windows in a container that is attached to the FOV, a controller, etc.
 * @lookup_key: If looking up the #XrdWindow by a key with
 * xrd_shell_lookup_window() should be enabled, set to != NULL.
 * Note that an #XrdWindow can be replaced by the overlay-scene switch.
 * Therefore the #XrdWindow should always be looked up instead of cached.
 */
void
xrd_shell_add_window (XrdShell  *self,
                      XrdWindow *window,
                      G3kObject *parent,
                      gboolean   draggable,
                      gpointer   lookup_key)
{
  G3kInteractionFlags flags = G3K_INTERACTION_HOVERABLE
                              | G3K_INTERACTION_DESTROY_WITH_PARENT;

  /* User can't drag child windows, they are attached to the parent.
   * The child window's position is managed by its parent, not the WM. */
  if (draggable)
    flags |= G3K_INTERACTION_DRAGGABLE | G3K_INTERACTION_MANAGED;

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_add_object (manager, G3K_OBJECT (window), flags);

  if (!(flags & G3K_INTERACTION_BUTTON))
    {
      GSettings *settings = g3k_settings_get_instance ("org.xrdesktop");
      gboolean   pin_new_window = g_settings_get_boolean (settings,
                                                          "pin-new-windows");
      xrd_window_set_pin (window, pin_new_window, FALSE);
    }

  if (self->pinned_only && !(flags & G3K_INTERACTION_BUTTON)
      && !xrd_window_is_pinned (window))
    {
      g3k_object_set_visibility (G3K_OBJECT (window), FALSE);
    }

  _add_window_callbacks (self, window);

  if (lookup_key != NULL)
    g_hash_table_insert (self->window_mapping, lookup_key, window);

  G3kObject *parent_node = parent ? parent : g3k_context_get_root (self->g3k);
  g3k_object_add_child (parent_node, G3K_OBJECT (window), 0);
}

/**
 * xrd_shell_lookup_window:
 * @self: The #XrdShell
 * @key: The key
 *
 * Returns: (transfer none) (nullable): a #XrdWindow
 */
XrdWindow *
xrd_shell_lookup_window (XrdShell *self, gpointer key)
{
  XrdWindow *window = g_hash_table_lookup (self->window_mapping, key);
  if (!window)
    {
      g_print ("Error looking up window %p\n", key);
      return NULL;
    }
  return window;
}

/**
 * xrd_shell_add_button:
 * @self: The #XrdShell
 * @button: The button (#XrdWindow) to add.
 * @position: World space position of the button.
 * @press_callback: (scope async): A function that will be called when the
 * button is grabbed.
 * @press_callback_data: User pointer passed to @press_callback.
 * @parent: Parent node in the scene graph. May be NULL to choose the root node.
 *
 * Buttons are special windows that can not be grabbed and dragged around.
 * Instead a button's press_callback is called on the grab action.
 */
void
xrd_shell_add_button (XrdShell           *self,
                      G3kButton          *button,
                      graphene_point3d_t *position,
                      GCallback           press_callback,
                      gpointer            press_callback_data,
                      G3kObject          *parent)
{
  graphene_matrix_t transform;
  graphene_matrix_init_translate (&transform, position);
  g3k_object_set_matrix (G3K_OBJECT (button), &transform);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_add_object (manager, G3K_OBJECT (button),
                                 G3K_INTERACTION_HOVERABLE
                                   | G3K_INTERACTION_DESTROY_WITH_PARENT
                                   | G3K_INTERACTION_BUTTON);

  if (press_callback != NULL)
    g_signal_connect (button, "grab-start-event", (GCallback) press_callback,
                      press_callback_data);

  _add_button_callbacks (self, button);

  if (parent)
    {
      g3k_object_add_child (parent, G3K_OBJECT (button), 0);
    }
  else
    {
      g3k_object_add_child (g3k_context_get_root (self->g3k),
                            G3K_OBJECT (button), 0);
    }
}

void
xrd_shell_set_pin (XrdShell *self, XrdWindow *win, gboolean pin)
{
  xrd_window_set_pin (win, pin, self->pinned_only);
}

void
xrd_shell_show_pinned_only (XrdShell *self, gboolean pinned_only)
{
  self->pinned_only = pinned_only;

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  GSList           *objects = g3k_object_manager_get_objects (manager);

  for (GSList *l = objects; l != NULL; l = l->next)
    {
      G3kObject *object = (G3kObject *) l->data;
      gboolean   pinned = xrd_window_is_pinned (XRD_WINDOW (object));
      gboolean   is_visible = !pinned_only || (pinned_only && pinned);
      g3k_object_set_visibility (object, is_visible);
    }

  if (self->button_pinned_only)
    {
      if (pinned_only)
        {
          g3k_button_set_icon (self->button_pinned_only,
                               "/icons/object-hidden-symbolic.svg");
        }
      else
        {
          g3k_button_set_icon (self->button_pinned_only,
                               "/icons/object-visible-symbolic.svg");
        }
    }
}

/**
 * xrd_shell_get_keyboard_window
 * @self: The #XrdShell
 *
 * Returns: (transfer none) (nullable): The window that is currently used for
 * keyboard input. Can be %NULL.
 */
XrdWindow *
xrd_shell_get_keyboard_window (XrdShell *self)
{
  return self->keyboard_window;
}

G3kContext *
xrd_shell_get_g3k (XrdShell *self)
{
  return self->g3k;
}

/**
 * xrd_shell_get_synth_hovered:
 * @self: The #XrdShell
 *
 * Returns: (transfer none) (nullable): If the controller used for synthesizing
 * input is hovering over an #XrdWindow, return this window, else %NULL.
 */
XrdWindow *
xrd_shell_get_synth_hovered (XrdShell *self)
{
  if (!self->input_synth)
    {
      g_print ("Error: No window hovered because synth is NULL\n");
      return NULL;
    }

  G3kController *controller = g3k_context_get_primary_controller (self->g3k);

  /* no controller, nothing hovered */
  if (controller == NULL)
    return NULL;

  G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);
  XrdWindow     *hovered_window = XRD_WINDOW (hover_state->object);

  return hovered_window;
}

/**
 * xrd_shell_get_desktop_cursor:
 * @self: The #XrdShell
 *
 * Returns: (transfer none): The #G3kCursor.
 */
G3kCursor *
xrd_shell_get_desktop_cursor (XrdShell *self)
{
  return self->cursor;
}

static void
_finalize (GObject *gobject)
{
  XrdShell *self = XRD_SHELL (gobject);

  g_object_unref (self->background);

  if (self->wm_control_container)
    _destroy_buttons (self);

  g_hash_table_unref (self->window_mapping);

  for (int i = 0; i < LAST_ACTIONSET; i++)
    {
      g_clear_object (&self->action_sets[i]);
      g_clear_object (&self->manifests[i]);
    }

  g_clear_object (&self->cursor);
  g_clear_object (&self->input_synth);

  g_clear_object (&self->g3k);
  g_clear_object (&self->wm_control_container);

  g_object_unref (self->keyboard);

  G_OBJECT_CLASS (xrd_shell_parent_class)->finalize (gobject);
}

static gboolean
_match_value_ptr (gpointer key, gpointer value, gpointer user_data)
{
  (void) key;
  return value == user_data;
}

/**
 * xrd_shell_remove_window:
 * @self: The #XrdShell
 * @window: The #XrdWindow to remove.
 *
 * Removes an #XrdWindow from the management of the #XrdShell and the
 * #G3kObjectManager.
 * Note that the #XrdWindow will not be destroyed by this function.
 */
void
xrd_shell_remove_window (XrdShell *self, XrdWindow *window)
{
  g_hash_table_foreach_remove (self->window_mapping, _match_value_ptr, window);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_remove_object (manager, G3K_OBJECT (window));

  /*
   * technically xrd_window_close () invalidates the hovered_object.
   * But we require xrd_shell_remove_window to be called, which is a better
   * place to clear the controller targets.
   * This is done to ensure g3k_object_manager_update_controller doesn't
   * access them'
   */
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);
      if (hover_state->object == G3K_OBJECT (window))
        hover_state->object = NULL;
    }

  /*
   * If the window is closed that the keyboard is operating on, the keyboard
   * should not do anything with the window anymore. Instead, just close the
   * keyboard.
   */

  if (self->keyboard_window == window)
    {
      g3k_keyboard_hide (self->keyboard);
      self->keyboard_window = NULL;
    }
}

/**
 * xrd_shell_get_input_synth:
 * @self: The #XrdShell
 *
 * Returns: (transfer none): A #XrdInputSynth.
 */
XrdInputSynth *
xrd_shell_get_input_synth (XrdShell *self)
{
  return self->input_synth;
}

gboolean
xrd_shell_poll_runtime_events (XrdShell *self)
{
  if (!self->g3k)
    return FALSE;

  gxr_context_poll_events (g3k_context_get_gxr (self->g3k));

  return TRUE;
}

static gboolean
_is_hovering (XrdShell *self)
{
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      if (g3k_controller_get_hover_state (controller)->object != NULL)
        return TRUE;
    }
  return FALSE;
}

static gboolean
_is_grabbing (XrdShell *self)
{
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      if (g3k_controller_get_grab_state (controller)->object != NULL)
        return TRUE;
    }
  return FALSE;
}

static gboolean
_is_grabbed (XrdShell *self, G3kObject *object)
{
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      G3kGrabState  *grab_state = g3k_controller_get_grab_state (controller);
      if (grab_state->object == object)
        return TRUE;
    }
  return FALSE;
}

static gboolean
_is_hovered (XrdShell *self, G3kObject *object)
{
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);
      if (hover_state->object == object)
        return TRUE;
    }
  return FALSE;
}

static void
_check_hover_grab (XrdShell *self)
{
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      GxrController *gxr_controller
        = g3k_controller_get_controller (controller);

      /* TODO: handle invalid pointer poses better */
      if (!gxr_controller_is_pointer_pose_valid (gxr_controller))
        continue;

      g3k_object_manager_update_controller (manager, controller);

      GSList *buttons = g3k_object_manager_get_buttons (manager);

      G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);

      gboolean is_button = g_slist_find (buttons, hover_state->object) != NULL;
      gboolean hovering_window_for_input = hover_state->object != NULL
                                           && !is_button;

      G3kGrabState *grab_state = g3k_controller_get_grab_state (controller);

      /* show cursor while synth controller hovers window, but doesn't grab */
      if (g3k_context_is_controller_primary (self->g3k, controller)
          && hovering_window_for_input && grab_state->object == NULL)
        g3k_object_set_visibility (G3K_OBJECT (self->cursor), TRUE);
    }
}

static void
_input_poll_cb (G3kContext *context, gpointer _self)
{
  (void) context;
  XrdShell *self = XRD_SHELL (_self);

  if (self->action_sets[WM_ACTIONSET] == NULL
      || self->action_sets[SYNTH_ACTIONSET] == NULL)
    {
      g_printerr ("Error: Action Set not created!\n");
      return;
    }

  /* self->action_sets[0] is wm actionset,
   * self->action_sets[1] is synth actionset.
   * wm actions are always updated, synth are added when hovering a window */
  gboolean poll_synth = _is_hovering (self) && !_is_grabbing (self);
  uint32_t count = poll_synth ? 2 : 1;

  if (!gxr_action_sets_poll (self->action_sets, count))
    {
      g_printerr ("Error polling actions\n");
      return;
    }

  _check_hover_grab (self);
}

static void
_perform_push_pull (XrdShell      *self,
                    GxrController *gxr_controller,
                    float          push_pull_strength,
                    float          ms_since_last_poll)
{
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          gxr_controller);
  G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);

  float factor = (float) self->scroll_to_push_ratio * push_pull_strength
                 * ms_since_last_poll * 0.001f;
  float new_dist = hover_state->distance * (1 + factor);

  if (new_dist < WINDOW_MIN_DIST || new_dist > WINDOW_MAX_DIST)
    return;

  hover_state->distance = new_dist;

  /* TODO: handle this in controller? */
  g3k_ray_set_length (g3k_controller_get_ray (controller),
                      hover_state->distance);
}

/**
 * _scale_object:
 * @grab_state: The #G3kGrabState to scale
 * @factor: Scale factor between -1.0 and +1.0
 * @update_rate_ms: The update rate in ms
 *
 * While dragging a window, scale the window *factor* times per second
 */
static void
_scale_object (G3kGrabState *grab_state, float factor, float update_rate_ms)
{
  if (grab_state->object == NULL)
    return;

  graphene_point3d_t scale;
  g3k_object_get_scale (grab_state->object, &scale);

  const float final_factor = factor * update_rate_ms * 0.001f;

  scale.x += scale.x * final_factor;
  scale.y += scale.y * final_factor;
  scale.z += scale.z * final_factor;

  g3k_object_set_scale (grab_state->object, &scale);

  /* Grab point is relative to window center so we can just scale it */
  grab_state->offset.x += grab_state->offset.x * final_factor;
  grab_state->offset.y += grab_state->offset.y * final_factor;
}

static void
_action_push_pull_scale_cb (GxrAction      *action,
                            GxrAnalogEvent *event,
                            XrdShell       *self)
{
  (void) action;
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  G3kGrabState *grab_state = g3k_controller_get_grab_state (controller);

  if (grab_state->object == NULL)
    return;

  double x_state = (double) graphene_vec3_get_x (&event->state);
  double y_state = (double) graphene_vec3_get_y (&event->state);

  /* go back to undecided when "stopping" current action,
   * to allow switching actions without letting go of the window. */
  if (fabs (x_state) < self->analog_threshold
      && fabs (y_state) < self->analog_threshold)
    {
      grab_state->transform_lock = XRD_TRANSFORM_LOCK_NONE;
      return;
    }

  if (grab_state->transform_lock == XRD_TRANSFORM_LOCK_NONE)
    {
      if (fabs (x_state) > fabs (y_state)
          && fabs (x_state) > self->analog_threshold)
        grab_state->transform_lock = XRD_TRANSFORM_LOCK_SCALE;

      else if (fabs (y_state) > fabs (x_state)
               && fabs (y_state) > self->analog_threshold)
        grab_state->transform_lock = XRD_TRANSFORM_LOCK_PUSH_PULL;
    }

  float ms_since_last_poll = g3k_context_get_ms_since_last_poll (self->g3k);
  if (grab_state->transform_lock == XRD_TRANSFORM_LOCK_SCALE)
    {
      double factor = x_state * self->scroll_to_scale_ratio;
      _scale_object (grab_state, (float) factor, ms_since_last_poll);
    }
  else if (grab_state->transform_lock == XRD_TRANSFORM_LOCK_PUSH_PULL)
    _perform_push_pull (self, event->controller,
                        graphene_vec3_get_y (&event->state),
                        ms_since_last_poll);
}

static void
_action_push_pull_cb (GxrAction *action, GxrAnalogEvent *event, XrdShell *self)
{
  (void) action;

  if (!event->active)
    return;

  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);
  G3kGrabState  *grab_state = g3k_controller_get_grab_state (controller);
  if (!grab_state->object)
    return;

  if (fabsf (graphene_vec3_get_y (&event->state))
      > (float) self->analog_threshold)
    {
      _perform_push_pull (self, event->controller,
                          graphene_vec3_get_y (&event->state),
                          g3k_context_get_ms_since_last_poll (self->g3k));
    }
}

static void
_action_grab_cb (GxrAction *action, GxrDigitalEvent *event, XrdShell *self)
{
  (void) action;
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  if (event->changed)
    {
      if (event->state == 1)
        g3k_controller_check_grab (controller);
      else
        g3k_controller_check_release (controller);
    }
}

static void
_action_menu_cb (GxrAction *action, GxrDigitalEvent *event, XrdShell *self)
{
  (void) action;
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  if (event->changed && event->state == 1
      && !g3k_controller_get_hover_state (controller)->object)
    {
      if (self->wm_control_container)
        {
          g_debug ("Destroying buttons!");
          _destroy_buttons (self);
        }
      else
        {
          g_debug ("Initializing buttons!");
          _init_buttons (self, event->controller);
        }
    }
}

typedef struct
{
  G3kGrabState         *grab_state;
  graphene_quaternion_t from;
  graphene_quaternion_t from_neg;
  graphene_quaternion_t to;
  float                 interpolate;
  gint64                last_timestamp;
} XrdOrientationTransition;

static gboolean
_interpolate_orientation_cb (gpointer _transition)
{
  XrdOrientationTransition *transition = (XrdOrientationTransition *)
    _transition;

  G3kGrabState *grab_state = transition->grab_state;

  graphene_quaternion_slerp (&transition->from, &transition->to,
                             transition->interpolate,
                             &grab_state->object_rotation);

  graphene_quaternion_slerp (&transition->from_neg, &transition->to,
                             transition->interpolate,
                             &grab_state->inverse_controller_rotation);

  gint64 now = g_get_monotonic_time ();
  float  ms_since_last = (float) (now - transition->last_timestamp) / 1000.f;
  transition->last_timestamp = now;

  /* in seconds */
  const float transition_duration = 0.2f;

  transition->interpolate += ms_since_last / 1000.f / transition_duration;

  if (transition->interpolate > 1)
    {
      graphene_quaternion_init_identity (&grab_state
                                            ->inverse_controller_rotation);
      graphene_quaternion_init_identity (&grab_state->object_rotation);
      g_free (transition);
      return FALSE;
    }

  return TRUE;
}

static void
_action_reset_orientation_cb (GxrAction       *action,
                              GxrDigitalEvent *event,
                              XrdShell        *self)
{
  (void) action;
  (void) self;

  if (!(event->changed && event->state == 1))
    return;

  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);
  G3kGrabState  *grab_state = g3k_controller_get_grab_state (controller);

  XrdOrientationTransition *transition
    = g_malloc (sizeof (XrdOrientationTransition));

  /* TODO: Check if animation is already in progress */

  transition->last_timestamp = g_get_monotonic_time ();
  transition->interpolate = 0.;
  transition->grab_state = grab_state;

  graphene_quaternion_init_identity (&transition->to);
  graphene_quaternion_init_from_quaternion (&transition->from,
                                            &grab_state->object_rotation);
  graphene_quaternion_init_from_quaternion (&transition->from_neg,
                                            &grab_state
                                               ->inverse_controller_rotation);

  g_timeout_add (10, _interpolate_orientation_cb, transition);
}

static void
_mark_windows_for_selection_mode (XrdShell *self);

static void
_window_grab_start_cb (G3kObject     *object,
                       G3kController *controller,
                       gpointer       _self)
{
  XrdShell *self = XRD_SHELL (_self);
  if (self->selection_mode)
    {
      gboolean pinned = xrd_window_is_pinned (XRD_WINDOW (object));
      /* in selection mode, windows are always visible */
      xrd_window_set_pin (XRD_WINDOW (object), !pinned, FALSE);
      _mark_windows_for_selection_mode (self);
      return;
    }

  /* don't grab if this window is already grabbed */
  if (_is_grabbed (self, object))
    return;

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_drag_start (manager, controller);

  if (g3k_context_is_controller_primary (self->g3k, controller))
    g3k_object_set_visibility (G3K_OBJECT (self->cursor), FALSE);
}

static void
_window_grab_cb (G3kObject *object, G3kGrabEvent *event, gpointer _self)
{
  (void) object;
  (void) event;
  (void) _self;
}

static void
_mark_windows_for_selection_mode (XrdShell *self)
{
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  if (self->selection_mode)
    {
      GSList *all = g3k_object_manager_get_objects (manager);
      for (GSList *l = all; l != NULL; l = l->next)
        {
          G3kObject *object = l->data;

          gboolean is_pinned = xrd_window_is_pinned (XRD_WINDOW (object));
          xrd_window_set_selection_color (XRD_WINDOW (object), is_pinned);

          g3k_object_set_visibility (object, TRUE);
        }
    }
  else
    {
      GSList *all = g3k_object_manager_get_objects (manager);
      for (GSList *l = all; l != NULL; l = l->next)
        {
          G3kObject *object = l->data;

          xrd_window_reset_color (XRD_WINDOW (object));
          if (self->pinned_only)
            {
              gboolean is_visible = xrd_window_is_pinned (XRD_WINDOW (object));
              g3k_object_set_visibility (object, is_visible);
            }
        }
    }
}

static void
_button_hover_cb (G3kObject *object, G3kHoverEvent *event, gpointer _self)
{
  (void) _self;
  (void) event;
  g3k_button_set_selection_color (G3K_BUTTON (object), TRUE);
}

static void
_window_hover_end_cb (G3kObject     *object,
                      G3kController *controller,
                      gpointer       _self)
{
  (void) object;
  XrdShell *self = XRD_SHELL (_self);

  XrdInputSynth *input_synth = xrd_shell_get_input_synth (self);
  xrd_input_synth_reset_press_state (input_synth);

  if (g3k_context_is_controller_primary (self->g3k, controller))
    g3k_object_set_visibility (G3K_OBJECT (self->cursor), FALSE);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  GSList           *buttons = g3k_object_manager_get_buttons (manager);

  gpointer next_hovered = g3k_controller_get_hover_state (controller)->object;

  gboolean next_hovered_is_button = next_hovered && XRD_IS_WINDOW (next_hovered)
                                    && g_slist_find (buttons, next_hovered);

  if (self->ignore_input && !next_hovered_is_button)
    g3k_controller_hide_pointers (controller);
}

static void
_button_hover_end_cb (G3kObject     *object,
                      G3kController *controller,
                      gpointer       _self)
{
  (void) controller;
  XrdShell *self = XRD_SHELL (_self);
  // XrdShellPrivate *priv = xrd_shell_get_instance_private (self);

  /* unmark if no controller is hovering over this button */
  if (!_is_hovered (self, object))
    g3k_button_reset_color (G3K_BUTTON (object));

  _window_hover_end_cb (object, controller, _self);
}

static void
_button_sphere_press_cb (XrdWindow     *window,
                         G3kController *controller,
                         gpointer       _self)
{
  (void) controller;
  (void) window;
  XrdShell         *self = XRD_SHELL (_self);
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_arrange_sphere (manager);
}

static void
_button_reset_press_cb (XrdWindow     *window,
                        G3kController *controller,
                        gpointer       _self)
{
  (void) controller;
  (void) window;
  XrdShell         *self = XRD_SHELL (_self);
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_arrange_reset (manager);
}

static void
_button_pinned_press_cb (XrdWindow     *button,
                         G3kController *controller,
                         gpointer       _self)
{
  (void) controller;
  (void) button;
  XrdShell *self = _self;

  if (self->selection_mode)
    return;

  xrd_shell_show_pinned_only (self, !self->pinned_only);
}

static void
_button_select_pinned_press_cb (XrdWindow     *button,
                                G3kController *controller,
                                gpointer       _self)
{
  (void) controller;
  (void) button;
  XrdShell *self = _self;
  self->selection_mode = !self->selection_mode;
  _mark_windows_for_selection_mode (self);

  if (self->selection_mode)
    {
      g3k_button_set_icon (self->button_selection_mode,
                           "/icons/object-select-symbolic.svg");
    }
  else
    {
      g3k_button_set_icon (self->button_selection_mode,
                           "/icons/view-pin-symbolic.svg");
    }
}

static void
_show_pointers (XrdShell *self)
{
  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    g3k_controller_show_pointers (l->data);
}

static void
_hide_pointers (XrdShell *self, gboolean except_button_hover)
{
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  GSList           *buttons = g3k_object_manager_get_buttons (manager);

  g_autoptr (GList) cs = g3k_context_get_controllers (self->g3k);
  for (GList *l = cs; l; l = l->next)
    {
      XrdWindow *window = XRD_WINDOW (g3k_controller_get_hover_state (l->data)
                                        ->object);

      if (!(except_button_hover && window && g_slist_find (buttons, window)))
        g3k_controller_hide_pointers (l->data);
    }
}

static void
_button_ignore_input_press_cb (XrdWindow     *button,
                               GxrController *controller,
                               gpointer       _self)
{
  (void) button;
  (void) controller;
  XrdShell *self = _self;

  self->ignore_input = !self->ignore_input;

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_set_hover_mode (manager, self->ignore_input
                                                ? G3K_HOVER_MODE_BUTTONS
                                                : G3K_HOVER_MODE_EVERYTHING);

  if (self->ignore_input)
    {
      g3k_button_set_icon (self->button_ignore_input,
                           "/icons/input-no-mouse-symbolic.svg");
    }
  else
    {
      g3k_button_set_icon (self->button_ignore_input,
                           "/icons/input-mouse-symbolic.svg");
    }

  if (self->ignore_input)
    _hide_pointers (self, TRUE);
  else
    _show_pointers (self);
}

static void
_grid_position (graphene_size_t   *size_meters,
                float              grid_rows,
                float              grid_columns,
                float              row,
                float              column,
                graphene_matrix_t *relative_transform)
{
  float grid_width = grid_columns * size_meters->width;
  float grid_height = grid_rows * size_meters->height;

  float y_offset = grid_height / 2.f - size_meters->height * row
                   - size_meters->height / 2.f;

  float x_offset = -grid_width / 2.f + size_meters->width * column
                   + size_meters->width / 2.f;

  graphene_point3d_t position;
  graphene_point3d_init (&position, x_offset, y_offset, 0);
  graphene_matrix_init_translate (relative_transform, &position);
}

static gboolean
_init_buttons (XrdShell *self, GxrController *controller)
{
  GxrContext       *gxr = g3k_context_get_gxr (self->g3k);
  GxrDeviceManager *dm = gxr_context_get_device_manager (gxr);
  GSList           *controllers = gxr_device_manager_get_controllers (dm);

  int valid_controller_count = 0;
  for (GSList *l = controllers; l; l = l->next)
    {
      GxrController *c = GXR_CONTROLLER (l->data);
      if (gxr_controller_is_pointer_pose_valid (c))
        valid_controller_count++;
    }

  /* Controller attached requires more than 1 controller to use.
   * Otherwise use head tracked menu. */
  int attach_controller = valid_controller_count > 1;

  g_debug ("%d controllers active, attaching menu to %s",
           valid_controller_count, attach_controller ? "hand" : "head");

  graphene_size_t size_meters;

  if (attach_controller)
    {
      graphene_size_init (&size_meters, 0.07f, 0.07f);
    }
  else
    {
      graphene_size_init (&size_meters, 0.25f, 0.25f);
    }

  float rows = 3;
  float columns = 2;

  self->wm_control_container = g3k_container_new (self->g3k);
  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->wm_control_container), 1);

  g3k_container_set_attachment (self->wm_control_container,
                                attach_controller
                                  ? G3K_CONTAINER_ATTACHMENT_HAND
                                  : G3K_CONTAINER_ATTACHMENT_HEAD,
                                controller);
  g3k_container_set_layout (self->wm_control_container, G3K_CONTAINER_RELATIVE);

  /* position where button is initially created doesn't matter. */
  graphene_point3d_t position = {.x = 0, .y = 0, .z = 0};

  graphene_matrix_t relative_transform;

  self->button_sphere = g3k_button_new (self->g3k, &size_meters);
  if (!self->button_sphere)
    return FALSE;

  g3k_button_set_icon (self->button_sphere, "/icons/align-sphere-symbolic.svg");
  xrd_shell_add_button (self, self->button_sphere, &position,
                        (GCallback) _button_sphere_press_cb, self, NULL);

  _grid_position (&size_meters, rows, columns, 0, 0, &relative_transform);
  g3k_container_add_child (self->wm_control_container,
                           G3K_OBJECT (self->button_sphere),
                           &relative_transform);

  self->button_reset = g3k_button_new (self->g3k, &size_meters);
  if (!self->button_reset)
    return FALSE;

  g3k_button_set_icon (self->button_reset, "/icons/edit-undo-symbolic.svg");
  xrd_shell_add_button (self, self->button_reset, &position,
                        (GCallback) _button_reset_press_cb, self, NULL);

  _grid_position (&size_meters, rows, columns, 0, 1, &relative_transform);
  g3k_container_add_child (self->wm_control_container,
                           G3K_OBJECT (self->button_reset),
                           &relative_transform);

  self->button_selection_mode = g3k_button_new (self->g3k, &size_meters);
  if (!self->button_selection_mode)
    return FALSE;

  g3k_button_set_icon (self->button_selection_mode,
                       "/icons/view-pin-symbolic.svg");
  xrd_shell_add_button (self, self->button_selection_mode, &position,
                        (GCallback) _button_select_pinned_press_cb, self, NULL);

  _grid_position (&size_meters, rows, columns, 1, 0, &relative_transform);
  g3k_container_add_child (self->wm_control_container,
                           G3K_OBJECT (self->button_selection_mode),
                           &relative_transform);

  self->button_pinned_only = g3k_button_new (self->g3k, &size_meters);
  if (!self->button_pinned_only)
    return FALSE;

  if (self->pinned_only)
    {

      g3k_button_set_icon (self->button_pinned_only,
                           "/icons/object-hidden-symbolic.svg");
    }
  else
    {
      g3k_button_set_icon (self->button_pinned_only,
                           "/icons/object-visible-symbolic.svg");
    }

  xrd_shell_add_button (self, self->button_pinned_only, &position,
                        (GCallback) _button_pinned_press_cb, self, NULL);

  _grid_position (&size_meters, rows, columns, 1, 1, &relative_transform);
  g3k_container_add_child (self->wm_control_container,
                           G3K_OBJECT (self->button_pinned_only),
                           &relative_transform);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_add_container (manager, self->wm_control_container);
  self->button_ignore_input = g3k_button_new (self->g3k, &size_meters);

  if (!self->button_ignore_input)
    return FALSE;

  if (self->ignore_input)
    {

      g3k_button_set_icon (self->button_ignore_input,
                           "/icons/input-no-mouse-symbolic.svg");
    }
  else
    {
      g3k_button_set_icon (self->button_ignore_input,
                           "/icons/input-mouse-symbolic.svg");
    }

  xrd_shell_add_button (self, self->button_ignore_input, &position,
                        (GCallback) _button_ignore_input_press_cb, self, NULL);

  _grid_position (&size_meters, rows, columns, 2, 0.5f, &relative_transform);
  g3k_container_add_child (self->wm_control_container,
                           G3K_OBJECT (self->button_ignore_input),
                           &relative_transform);

  if (!attach_controller)
    {
      const float distance = 2.0f;
      g3k_container_center_view (self->wm_control_container, distance);
      g3k_container_set_distance (self->wm_control_container, distance);
    }

  return TRUE;
}

static void
_destroy_buttons (XrdShell *self)
{
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_remove_object (manager, G3K_OBJECT (self->button_sphere));
  g_clear_object (&self->button_sphere);
  g3k_object_manager_remove_object (manager, G3K_OBJECT (self->button_reset));
  g_clear_object (&self->button_reset);
  g3k_object_manager_remove_object (manager,
                                    G3K_OBJECT (self->button_pinned_only));
  g_clear_object (&self->button_pinned_only);
  g3k_object_manager_remove_object (manager,
                                    G3K_OBJECT (self->button_selection_mode));
  g_clear_object (&self->button_selection_mode);
  g3k_object_manager_remove_object (manager,
                                    G3K_OBJECT (self->button_ignore_input));
  g_clear_object (&self->button_ignore_input);

  g3k_object_manager_remove_container (manager, self->wm_control_container);

  g_clear_object (&self->wm_control_container);
}

static void
_keyboard_press_cb (G3kKeyboard *keyboard,
                    G3kKeyEvent *event,
                    gpointer     user_data)
{
  (void) keyboard;
  XrdShell *self = XRD_SHELL (user_data);
  g_signal_emit (self, signals[KEYBOARD_PRESS_EVENT], 0, event);
}

static void
_keyboard_pre_show_cb (G3kKeyboard *keyboard, gpointer _self)
{
  (void) keyboard;
  XrdShell *self = XRD_SHELL (_self);
  (void) self;
  g_debug ("XrdShell: showing keyboard");
}

static void
_keyboard_post_hide_cb (G3kKeyboard *keyboard, gpointer _self)
{
  (void) keyboard;
  XrdShell *self = XRD_SHELL (_self);
  self->keyboard_window = NULL;
  g_debug ("XrdShell: hiding keyboard");
}

G3kKeyboard *
xrd_shell_get_keyboard (XrdShell *self)
{
  return self->keyboard;
}

static void
_action_show_keyboard_cb (GxrAction       *action,
                          GxrDigitalEvent *event,
                          XrdShell        *self)
{
  (void) action;
  if (self->ignore_input)
    return;

  if (!event->state && event->changed)
    {
      if (g3k_keyboard_is_visible (self->keyboard))
        {
          g3k_keyboard_hide (self->keyboard);
        }
      else
        {
          G3kController *controller
            = g3k_context_get_primary_controller (self->g3k);

          /* window hovered by the synthing controller receives input */
          self->keyboard_window
            = XRD_WINDOW (g3k_controller_get_hover_state (controller)->object);

          if (!self->keyboard_window)
            return;

          g3k_keyboard_show (self->keyboard);
        }
    }
}

static void
_window_hover_cb (XrdWindow *window, G3kHoverEvent *event, XrdShell *self)
{
  G3kHoverState *hover_state = g3k_controller_get_hover_state (event
                                                                 ->controller);
  hover_state->object = G3K_OBJECT (window);

  GxrController *gxr_controller
    = g3k_controller_get_controller (event->controller);

  if (g3k_context_is_controller_primary (self->g3k, event->controller))
    {
      graphene_matrix_t pointer_pose;
      gxr_controller_get_pointer_pose (gxr_controller, &pointer_pose);

      xrd_input_synth_move_cursor (self->input_synth, window, &pointer_pose,
                                   &event->point);

      g3k_cursor_update (self->cursor, G3K_OBJECT (window), &event->point);

      if (hover_state->object != G3K_OBJECT (window))
        xrd_input_synth_reset_scroll (self->input_synth);
    }
}

static void
_window_hover_start_cb (G3kObject     *object,
                        GxrController *gxr_controller,
                        XrdShell      *self)
{
  (void) object;
  G3kController *xrdc = g3k_context_get_controller (self->g3k, gxr_controller);
  if (self->ignore_input)
    g3k_controller_show_pointers (xrdc);
}

static void
_manager_no_hover_cb (G3kObjectManager *manager,
                      G3kNoHoverEvent  *event,
                      XrdShell         *self)
{
  (void) manager;
  if (g3k_context_is_controller_primary (self->g3k, event->controller))
    xrd_input_synth_reset_scroll (self->input_synth);
}

static void
_synth_click_cb (XrdInputSynth *synth, XrdClickEvent *event, XrdShell *self)
{
  (void) synth;
  G3kHoverState *hover_state = g3k_controller_get_hover_state (event
                                                                 ->controller);
  if (!hover_state->object)
    return;

  event->object = hover_state->object;

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  GSList           *buttons = g3k_object_manager_get_buttons (manager);
  gboolean is_button = g_slist_find (buttons, hover_state->object) != NULL;

  if (!is_button)
    {
      /* in window selection mode don't handle clicks on non-button windows */
      if (self->selection_mode)
        return;
      else
        g_signal_emit (self, signals[CLICK_EVENT], 0, event);
    }

  if (event->button == LEFT_BUTTON && event->state)
    {
      g3k_tip_animate_pulse (g3k_controller_get_tip (event->controller));

      if (is_button)
        g3k_object_emit_grab_start (hover_state->object, event->controller);
    }
}

static void
_synth_move_cursor_cb (XrdInputSynth      *synth,
                       XrdMoveCursorEvent *event,
                       XrdShell           *self)
{
  if (self->selection_mode)
    return;

  (void) synth;
  if (!event->ignore)
    g_signal_emit (self, signals[MOVE_CURSOR_EVENT], 0, event);
}

static void
_add_button_callbacks (XrdShell *self, G3kButton *button)
{
  g_signal_connect (button, "hover-event", (GCallback) _button_hover_cb, self);

  g_signal_connect (button, "hover-start-event",
                    (GCallback) _window_hover_start_cb, self);

  g_signal_connect (button, "hover-end-event", (GCallback) _button_hover_end_cb,
                    self);
}

static void
_add_window_callbacks (XrdShell *self, XrdWindow *window)
{
  g_signal_connect (window, "grab-start-event",
                    (GCallback) _window_grab_start_cb, self);
  g_signal_connect (window, "grab-event", (GCallback) _window_grab_cb, self);
  g_signal_connect (window, "hover-start-event",
                    (GCallback) _window_hover_start_cb, self);
  g_signal_connect (window, "hover-event", (GCallback) _window_hover_cb, self);
  g_signal_connect (window, "hover-end-event", (GCallback) _window_hover_end_cb,
                    self);
}

static void
xrd_shell_init (XrdShell *self)
{
  self->window_mapping = g_hash_table_new (g_direct_hash, g_direct_equal);

  g3k_settings_connect_and_apply (G_CALLBACK (g3k_settings_update_double_val),
                                  "org.xrdesktop", "scroll-to-push-ratio",
                                  &self->scroll_to_push_ratio);
  g3k_settings_connect_and_apply (G_CALLBACK (g3k_settings_update_double_val),
                                  "org.xrdesktop", "scroll-to-scale-ratio",
                                  &self->scroll_to_scale_ratio);
  g3k_settings_connect_and_apply (G_CALLBACK (g3k_settings_update_double_val),
                                  "org.xrdesktop", "analog-threshold",
                                  &self->analog_threshold);
  g3k_settings_connect_and_apply (G_CALLBACK (g3k_settings_update_gboolean_val),
                                  "org.xrdesktop", "show-only-pinned-startup",
                                  &self->pinned_only);

  self->keyboard_window = NULL;
  self->keyboard_press_signal = 0;
  self->keyboard_close_signal = 0;
  self->selection_mode = FALSE;
  self->ignore_input = FALSE;
  for (int i = 0; i < LAST_ACTIONSET; i++)
    {
      self->manifests[i] = NULL;
      self->action_sets[i] = NULL;
    }
  self->cursor = NULL;
  self->wm_control_container = NULL;

  self->g3k = NULL;

  self->background = NULL;
}

static void
_update_grab_window_threshold (GSettings *settings,
                               gchar     *key,
                               gpointer   _action)
{
  GxrAction *action = GXR_ACTION (_action);

  float threshold = (float) g_settings_get_double (settings, key);

  gxr_action_set_digital_from_float_threshold (action, threshold);
}

static void
_controller_activate_cb (G3kContext    *g3k,
                         G3kController *controller,
                         gpointer       _self)
{
  (void) g3k;
  XrdShell *self = XRD_SHELL (_self);
  g3k_controller_set_haptic_action (controller, self->haptic_action);
}

static GxrActionSet *
_create_wm_action_set (XrdShell *self, GxrManifest *manifest)
{
  GxrContext   *gxr = g3k_context_get_gxr (self->g3k);
  GxrActionSet *set = gxr_action_set_new_from_url (gxr, manifest,
                                                   "/actions/wm");

  GxrDeviceManager *dm = gxr_context_get_device_manager (gxr);
  gxr_device_manager_connect_pose_actions (dm, set, "/actions/wm/in/hand_pose",
                                           "/actions/wm/in/"
                                           "hand_pose_hand_grip");

  GxrAction *grab_window
    = gxr_action_set_connect_digital_from_float (set,
                                                 "/actions/wm/in/grab_window",
                                                 0.25f,
                                                 "/actions/wm/out/haptic",
                                                 (GCallback) _action_grab_cb,
                                                 self);

  self->haptic_action = gxr_action_get_haptic_action (grab_window);

  g_signal_connect (self->g3k, "controller-activate",
                    G_CALLBACK (_controller_activate_cb), self);

  gxr_action_set_connect (set, GXR_ACTION_DIGITAL,
                          "/actions/wm/in/reset_orientation",
                          (GCallback) _action_reset_orientation_cb, self);
  gxr_action_set_connect (set, GXR_ACTION_DIGITAL, "/actions/wm/in/menu",
                          (GCallback) _action_menu_cb, self);
  gxr_action_set_connect (set, GXR_ACTION_VEC2F,
                          "/actions/wm/in/push_pull_scale",
                          (GCallback) _action_push_pull_scale_cb, self);
  gxr_action_set_connect (set, GXR_ACTION_VEC2F, "/actions/wm/in/push_pull",
                          (GCallback) _action_push_pull_cb, self);

  gxr_action_set_connect (set, GXR_ACTION_DIGITAL,
                          "/actions/wm/in/show_keyboard",
                          (GCallback) _action_show_keyboard_cb, self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_grab_window_threshold),
                                  "org.xrdesktop", "grab-window-threshold",
                                  grab_window);

  return set;
}

static void
_overlay_cb (GxrContext *context, GxrOverlayEvent *event, XrdShell *self)
{
  (void) context;
  g3k_object_set_visibility (G3K_OBJECT (self->background),
                             !event->main_session_visible);
}

static void
_init_input_callbacks (XrdShell *self)
{
  self->input_synth = xrd_input_synth_new (self->g3k);
  self->button_sphere = NULL;
  self->button_reset = NULL;
  self->button_pinned_only = NULL;
  self->button_selection_mode = NULL;
  self->button_ignore_input = NULL;
  self->wm_control_container = NULL;

  GxrManifest *manifest = gxr_manifest_new ("/res/bindings", "actions.json");
  if (!manifest)
    {
      g_print ("Failed to load wm action manifest!\n");
      return;
    }

  self->action_sets[WM_ACTIONSET] = _create_wm_action_set (self, manifest);
  self->action_sets[SYNTH_ACTIONSET]
    = xrd_input_synth_create_action_set (self->input_synth, manifest);

  GxrContext *gxr = g3k_context_get_gxr (self->g3k);

  gxr_context_attach_action_sets (gxr, self->action_sets, 2);

  g_signal_connect (g3k_context_get_gxr (self->g3k), "overlay-event",
                    (GCallback) _overlay_cb, self);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g_signal_connect (manager, "no-hover-event", (GCallback) _manager_no_hover_cb,
                    self);

  g_signal_connect (self->input_synth, "click-event",
                    (GCallback) _synth_click_cb, self);
  g_signal_connect (self->input_synth, "move-cursor-event",
                    (GCallback) _synth_move_cursor_cb, self);

  g_signal_connect (self->g3k, "input-poll", G_CALLBACK (_input_poll_cb), self);
}

static gboolean
_init_widgets (XrdShell *self);

XrdShell *
xrd_shell_new_from_g3k (G3kContext *g3k)
{
  XrdShell *self = (XrdShell *) g_object_new (XRD_TYPE_SHELL, 0);

  self->g3k = g3k;

  if (!_init_widgets (self))
    {
      g_object_unref (self);
      g_printerr ("Could not init Widgets.\n");
      return NULL;
    }

  _init_input_callbacks (self);
  _init_keyboard (self);

  return self;
}

static gboolean
_init_widgets (XrdShell *self)
{
  self->background = g3k_background_new (self->g3k);

  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->background), 0);

  self->cursor = g3k_cursor_new (self->g3k);
  g3k_object_set_visibility (G3K_OBJECT (self->cursor), FALSE);

  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->cursor), UINT32_MAX);

  return TRUE;
}
