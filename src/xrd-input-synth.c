/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "xrd-input-synth.h"

#include <g3k.h>
#include <gdk/gdk.h>

#include "xrd-shake-compensator.h"

struct _XrdInputSynth
{
  GObject parent;

  G3kContext *g3k;

  /* hover_position is relative to hover_window */
  graphene_point_t hover_position;
  XrdWindow       *hover_window;

  /* bitmask */
  uint32_t button_press_state;

  graphene_vec3_t scroll_accumulator;

  float scroll_threshold;

  XrdShakeCompensator *compensator;
  gboolean             compensator_enabled;
};

G_DEFINE_TYPE (XrdInputSynth, xrd_input_synth, G_TYPE_OBJECT)

enum
{
  CLICK_EVENT,
  MOVE_CURSOR_EVENT,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = {0};

static void
_finalize (GObject *gobject);

static void
xrd_input_synth_class_init (XrdInputSynthClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = _finalize;

  signals[CLICK_EVENT] = g_signal_new ("click-event", G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                       G_TYPE_NONE, 1,
                                       GDK_TYPE_EVENT
                                         | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[MOVE_CURSOR_EVENT] = g_signal_new ("move-cursor-event",
                                             G_TYPE_FROM_CLASS (klass),
                                             G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                             NULL, G_TYPE_NONE, 1,
                                             GDK_TYPE_EVENT
                                               | G_SIGNAL_TYPE_STATIC_SCOPE);
}

static void
_controller_primary_changed_cb (G3kContext    *g3k,
                                G3kController *controller,
                                gpointer       _self)
{
  (void) g3k;
  (void) controller;
  XrdInputSynth *self = XRD_INPUT_SYNTH (_self);
  xrd_input_synth_reset_scroll (self);
  xrd_input_synth_reset_press_state (self);
}

XrdInputSynth *
xrd_input_synth_new (G3kContext *g3k)
{
  XrdInputSynth *self = (XrdInputSynth *) g_object_new (XRD_TYPE_INPUT_SYNTH,
                                                        0);
  self->g3k = g_object_ref (g3k);

  g_signal_connect (g3k, "controller-primary-changed",
                    (GCallback) _controller_primary_changed_cb, self);

  return self;
}

static void
_finalize (GObject *gobject)
{
  XrdInputSynth *self = XRD_INPUT_SYNTH (gobject);
  g_object_unref (self->compensator);
  g_object_unref (self->g3k);

  G_OBJECT_CLASS (xrd_input_synth_parent_class)->finalize (gobject);
}

static gboolean
_is_compensate_shake (XrdInputSynthButton button)
{
  // clang-format off
  return
    button == LEFT_BUTTON ||
    button == RIGHT_BUTTON ||
    button == MIDDLE_BUTTON;
  // clang-format on
}

static void
_emit_click (XrdInputSynth      *self,
             graphene_point_t   *position,
             XrdInputSynthButton button,
             gboolean            state)
{
  /* Button press and release only start and stop prediction.
   * If necessary, the prediction queue is replayed in mouse move. */
  if (state && _is_compensate_shake (button) && self->compensator_enabled)
    xrd_shake_compensator_start_recording (self->compensator, button);
  else if (!state
           && button == xrd_shake_compensator_get_button (self->compensator))
    xrd_shake_compensator_reset (self->compensator);

  XrdClickEvent click_event = {
    .position = position,
    .button = (XrdInputSynthButton) button,
    .state = state,
    .controller = g3k_context_get_primary_controller (self->g3k),
  };
  g_signal_emit (self, signals[CLICK_EVENT], 0, &click_event);
}

/**
 * xrd_input_synth_reset_press_state:
 * @self: The #XrdInputSynth
 *
 * Issue a button release event for every button that previously was used for a
 * button press event, but has not been released yet.
 *
 * When calling this function, also consider xrd_input_synth_reset_scroll().
 */
void
xrd_input_synth_reset_press_state (XrdInputSynth *self)
{
  if (self && self->button_press_state)
    {
      /*g_print ("End hover, button mask %d...\n", self->button_press_state); */
      for (XrdInputSynthButton button = 1; button <= 8; button++)
        {
          gboolean pressed = (self->button_press_state & 1 << button) != 0;
          if (pressed)
            {
              g_print ("Released button %d\n", button);
              _emit_click (self, &self->hover_position, button, FALSE);
            }
        }
      self->button_press_state = 0;
    }
}

static void
_process_click (XrdInputSynth      *self,
                GxrDigitalEvent    *event,
                XrdInputSynthButton button)
{
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  if (!g3k_context_is_controller_primary (self->g3k, controller))
    return;

  if (event->changed)
    {
      _emit_click (self, &self->hover_position, button, event->state);

      if (event->state)
        self->button_press_state |= 1 << button;
      else
        self->button_press_state &= ~(1 << button);
    }
}

static void
_action_left_click_cb (GxrAction       *action,
                       GxrDigitalEvent *event,
                       XrdInputSynth   *self)
{
  (void) action;

  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  /* when left clicking with a controller that is *not* used to do input
   * synth, make this controller do input synth */
  if (event->state
      && !g3k_context_is_controller_primary (self->g3k, controller))
    {
      g3k_context_make_controller_primary (self->g3k, controller);
      return;
    }

  _process_click (self, event, LEFT_BUTTON);
}

static void
_action_right_click_cb (GxrAction       *action,
                        GxrDigitalEvent *event,
                        XrdInputSynth   *self)
{
  (void) action;
  _process_click (self, event, RIGHT_BUTTON);
}

static void
_action_middle_click_cb (GxrAction       *action,
                         GxrDigitalEvent *event,
                         XrdInputSynth   *self)
{
  (void) action;
  _process_click (self, event, MIDDLE_BUTTON);
}

static void
_do_scroll (XrdInputSynth *self, int32_t steps_x, int32_t steps_y)
{
  for (int32_t i = 0; i < abs (steps_y); i++)
    {
      XrdInputSynthButton btn;
      if (steps_y > 0)
        btn = SCROLL_UP;
      else
        btn = SCROLL_DOWN;
      _emit_click (self, &self->hover_position, btn, TRUE);
      _emit_click (self, &self->hover_position, btn, FALSE);
    }

  for (int32_t i = 0; i < abs (steps_x); i++)
    {
      XrdInputSynthButton btn;
      if (steps_x < 0)
        btn = SCROLL_LEFT;
      else
        btn = SCROLL_RIGHT;
      _emit_click (self, &self->hover_position, btn, TRUE);
      _emit_click (self, &self->hover_position, btn, FALSE);
    }
}

/*
 * When the touchpad is touched, start adding up movement.
 * If movement is over threshold, create a scroll event and reset
 * scroll_accumulator.
 */

static void
_action_scroll_cb (GxrAction      *action,
                   GxrAnalogEvent *event,
                   XrdInputSynth  *self)
{
  (void) action;

  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  if (!g3k_context_is_controller_primary (self->g3k, controller))
    return;

  static graphene_vec3_t last_touch_pos;
  gboolean initial_touch = graphene_vec3_get_x (&last_touch_pos) == 0.0f
                           && graphene_vec3_get_y (&last_touch_pos) == 0.0f;
  graphene_vec3_init_from_vec3 (&last_touch_pos, &event->state);

  /* When stopping to touch the touchpad we get a deltea from where the
   * touchpad is touched to (0,0). Ignore this bogus delta. */
  if (graphene_vec3_get_x (&event->state) == 0.0f
      && graphene_vec3_get_y (&event->state) == 0.0f)
    return;

  /* When starting to touch the touchpad, we get a delta from (0,0) to where
   * the touchpad is touched. Ignore this bogus delta. */
  if (initial_touch)
    return;

  graphene_vec3_add (&self->scroll_accumulator, &event->delta,
                     &self->scroll_accumulator);

  float x_acc = graphene_vec3_get_x (&self->scroll_accumulator);
  float y_acc = graphene_vec3_get_y (&self->scroll_accumulator);

  /*
   * Scroll as many times as the threshold has been exceeded.
   * e.g. user scrolled 0.32 with threshold of 0.1 -> scroll 3 times.
   */

  int32_t steps_x = (int32_t) (x_acc / self->scroll_threshold);
  int32_t steps_y = (int32_t) (y_acc / self->scroll_threshold);

  g_assert (self->scroll_threshold > 0);
  g_assert (steps_x != INT32_MAX);
  g_assert (steps_y != INT32_MAX);

  g_assert (abs (steps_x) < 100);
  g_assert (abs (steps_y) < 100);

  /*
   * We need to keep the rest in the accumulator to not lose part of the
   * user's movement e.g. 0.32: -> 0.2 and -0.32 -> -0.2
   */
  float rest_x = x_acc - (float) steps_x * self->scroll_threshold;
  float rest_y = y_acc - (float) steps_y * self->scroll_threshold;
  graphene_vec3_init (&self->scroll_accumulator, rest_x, rest_y, 0);

  _do_scroll (self, steps_x, steps_y);
}

void
xrd_input_synth_move_cursor (XrdInputSynth      *self,
                             XrdWindow          *window,
                             graphene_matrix_t  *controller_pose,
                             graphene_point3d_t *intersection)
{
  graphene_point_t intersection_pixels;
  xrd_window_get_pixel_intersection (window, intersection,
                                     &intersection_pixels);

  XrdMoveCursorEvent event = {
    .window = window,
    .position = &intersection_pixels,
    .ignore = FALSE,
  };

  graphene_point_init_from_point (&self->hover_position, &intersection_pixels);
  self->hover_window = window;

  if (xrd_shake_compensator_is_recording (self->compensator))
    {
      xrd_shake_compensator_record (self->compensator, &intersection_pixels);

      gboolean is_drag = xrd_shake_compensator_is_drag (self->compensator,
                                                        self->hover_window,
                                                        controller_pose,
                                                        intersection);

      /* If we don't know yet, move cursor in VR pretending to be responsive.
       * If we predict drag, replay queue which contains "start of the drag".
       * If we predict click, queue only contains "shake" which we discard. */
      if (is_drag)
        {
          xrd_shake_compensator_replay_move_queue (self->compensator, self,
                                                   signals[MOVE_CURSOR_EVENT],
                                                   self->hover_window);
          xrd_shake_compensator_reset (self->compensator);
        }
      else
        {
          event.ignore = TRUE;
        }
    }

  g_signal_emit (self, signals[MOVE_CURSOR_EVENT], 0, &event);
}

static void
_update_scroll_threshold (GSettings *settings, gchar *key, gpointer _self)
{
  XrdInputSynth *self = _self;
  self->scroll_threshold = (float) g_settings_get_double (settings, key);
}

static void
_update_shake_compensation_enabled (GSettings     *settings,
                                    gchar         *key,
                                    XrdInputSynth *self)
{
  self->compensator_enabled = g_settings_get_boolean (settings, key);
  if (!self->compensator_enabled)
    xrd_shake_compensator_reset (self->compensator);
}

/**
 * xrd_input_synth_create_action_set:
 * @self: the #XrdInputSynth
 *
 * Returns: (transfer full): an #GxrActionSet that will be centrally owned by,
 * updated in and destroyed in #XrdShell.
 */
GxrActionSet *
xrd_input_synth_create_action_set (XrdInputSynth *self, GxrManifest *manifest)
{
  GxrActionSet *set
    = gxr_action_set_new_from_url (g3k_context_get_gxr (self->g3k), manifest,
                                   "/actions/mouse_synth");

  gxr_action_set_connect (set, GXR_ACTION_DIGITAL,
                          "/actions/mouse_synth/in/left_click",
                          (GCallback) _action_left_click_cb, self);
  gxr_action_set_connect (set, GXR_ACTION_DIGITAL,
                          "/actions/mouse_synth/in/right_click",
                          (GCallback) _action_right_click_cb, self);
  gxr_action_set_connect (set, GXR_ACTION_DIGITAL,
                          "/actions/mouse_synth/in/middle_click",
                          (GCallback) _action_middle_click_cb, self);
  gxr_action_set_connect (set, GXR_ACTION_VEC2F,
                          "/actions/mouse_synth/in/scroll",
                          (GCallback) _action_scroll_cb, self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_scroll_threshold),
                                  "org.xrdesktop", "scroll-threshold", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_shake_compensation_enabled),
                                  "org.xrdesktop", "shake-compensation-enabled",
                                  self);

  return set;
}

static void
xrd_input_synth_init (XrdInputSynth *self)
{
  self->button_press_state = 0;
  graphene_point_init (&self->hover_position, 0, 0);

  self->compensator = xrd_shake_compensator_new ();

  self->scroll_threshold = 0.1f;
  self->compensator_enabled = TRUE;
}

/**
 * xrd_input_synth_reset_scroll:
 * @self: The #XrdInputSynth
 *
 * Resets the internal state of the scrolling, so the in-flight scroll distance
 * on the touchpad is discarded.
 *
 * When calling this function, also consider xrd_input_synth_reset_press_state()
 */
void
xrd_input_synth_reset_scroll (XrdInputSynth *self)
{
  graphene_vec3_init (&self->scroll_accumulator, 0, 0, 0);
}
