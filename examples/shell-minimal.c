/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib-unix.h>
#include <glib.h>

#include <xrd.h>

typedef struct Example
{
  GMainLoop *loop;
  XrdShell  *shell;
  G3kButton *quit_button;
} Example;

static gboolean
_sigint_cb (gpointer _self)
{
  Example *self = (Example *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static void
_button_quit_press_cb (XrdWindow     *window,
                       GxrController *controller,
                       gpointer       _self)
{
  (void) controller;
  (void) window;
  Example *self = _self;
  g_main_loop_quit (self->loop);
}

static void
_init_buttons (Example *self)
{
  graphene_point3d_t button_pos = {.x = -2.5f, .y = 0.5f, .z = -2.5f};
  graphene_size_t    size_meters = {.6f, .6f};

  gchar *quit_str[] = {"Quit"};
  self->quit_button = g3k_button_new (xrd_shell_get_g3k (self->shell),
                                      &size_meters);
  if (!self->quit_button)
    return;

  g3k_button_set_text (self->quit_button, 1, quit_str);

  xrd_shell_add_button (self->shell, self->quit_button, &button_pos,
                        (GCallback) _button_quit_press_cb, self, NULL);
}

static void
_cleanup (Example *self)
{
  g_object_unref (self->shell);
  self->shell = NULL;

  g_print ("Cleaned up!\n");
}

static void
_shutdown_cb (GxrContext *context, Example *self)
{
  (void) context;
  g_main_loop_quit (self->loop);
}

static gboolean
_init_example (Example *self)
{
  if (!self->shell)
    {
      g_printerr ("XrdShell did not initialize correctly.\n");
      return FALSE;
    }

  G3kContext *g3k = xrd_shell_get_g3k (self->shell);
  g_signal_connect (g3k, "shutdown", G_CALLBACK (_shutdown_cb), self);

  _init_buttons (self);

  g_unix_signal_add (SIGINT, _sigint_cb, self);

  return TRUE;
}

int
main ()
{
  Example self = {
    .loop = g_main_loop_new (NULL, FALSE),
    .shell = xrd_shell_new (),
  };

  if (!_init_example (&self))
    return EXIT_FAILURE;

  /* start glib main loop */
  g_main_loop_run (self.loop);

  /* don't clean up when quitting during switching */
  if (self.shell != NULL)
    _cleanup (&self);

  g_main_loop_unref (self.loop);

  return EXIT_SUCCESS;
}
