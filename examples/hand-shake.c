/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib-unix.h>
#include <glib.h>

#include <xrd.h>

typedef struct Example
{
  GMainLoop *loop;
  XrdShell  *shell;
  guint64    click_source;
  guint64    move_source;
  gboolean   shutdown;

  XrdWindow     *canvas;
  GdkPixbuf     *canvas_pixbuf;
  GulkanTexture *canvas_texture;

  G3kButton *help_button;
  G3kButton *result_button;
  G3kButton *toggle_button;

  XrdInputSynthButton pressed_button;
  graphene_point_t    start;
  float               max_shake;
} Example;

static gboolean
_sigint_cb (gpointer _self)
{
  Example *self = (Example *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static void
_cleanup (Example *self);

static GdkPixbuf *
_create_draw_pixbuf (VkExtent2D extent)
{
  guchar *pixels = (guchar *) malloc (sizeof (guchar) * extent.height
                                      * extent.width * 4);
  memset (pixels, 255, extent.height * extent.width * 4 * sizeof (guchar));

  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_data (pixels, GDK_COLORSPACE_RGB,
                                                TRUE, 8, (int) extent.width,
                                                (int) extent.height,
                                                4 * (int) extent.width, NULL,
                                                NULL);
  return pixbuf;
}

typedef struct ColorRGBA
{
  guchar r;
  guchar g;
  guchar b;
  guchar a;
} ColorRGBA;

static void
_place_pixel (guchar    *pixels,
              int        n_channels,
              int        rowstride,
              int        x,
              int        y,
              ColorRGBA *color)
{
  guchar *p = pixels + y * rowstride + x * n_channels;

  p[0] = color->r;
  p[1] = color->g;
  p[2] = color->b;
  p[3] = color->a;
}

static gboolean
_draw_at_2d_position (Example          *self,
                      graphene_point_t *position_2d,
                      ColorRGBA        *color,
                      uint32_t          brush_radius)
{
  static GMutex paint_mutex;

  int     n_channels = gdk_pixbuf_get_n_channels (self->canvas_pixbuf);
  int     rowstride = gdk_pixbuf_get_rowstride (self->canvas_pixbuf);
  guchar *pixels = gdk_pixbuf_get_pixels (self->canvas_pixbuf);

  g_mutex_lock (&paint_mutex);

  for (float x = position_2d->x - (float) brush_radius;
       x <= position_2d->x + (float) brush_radius; x++)
    {
      for (float y = position_2d->y - (float) brush_radius;
           y <= position_2d->y + (float) brush_radius; y++)
        {
          graphene_vec2_t put_position;
          graphene_vec2_init (&put_position, x, y);

          graphene_vec2_t brush_center;
          graphene_point_to_vec2 (position_2d, &brush_center);

          graphene_vec2_t distance;
          graphene_vec2_subtract (&put_position, &brush_center, &distance);

          if ((uint32_t) (graphene_vec2_length (&distance)) < brush_radius)
            _place_pixel (pixels, n_channels, rowstride, (int) x, (int) y,
                          color);
        }
    }

  if (!gulkan_texture_upload_pixbuf (self->canvas_texture, self->canvas_pixbuf,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL))
    return FALSE;

  g_mutex_unlock (&paint_mutex);

  return TRUE;
}

static gboolean
_get_compensation ()
{
  GSettings *s = g_settings_new ("org.xrdesktop");
  gboolean   on = g_settings_get_boolean (s, "shake-compensation-enabled");
  return on;
}

static void
_set_compensation (gboolean on)
{
  GSettings *s = g_settings_new ("org.xrdesktop");
  g_settings_set_boolean (s, "shake-compensation-enabled", on);
}

static void
_toggle_press_cb (XrdWindow *button, GxrController *controller, gpointer _self)
{
  (void) button;
  (void) controller;

  Example *self = _self;

  gboolean on = _get_compensation ();
  g_print ("Hand Shake Compensation: %s\n", on ? "ON" : "OFF");

  _set_compensation (!on);

  on = _get_compensation ();
  g_print ("Toggled Hand Shake Compensation to: %s\n", on ? "ON" : "OFF");

  gchar *toggle_string[] = {"Comp.", on ? "ON" : "OFF"};
  g3k_button_set_text (self->toggle_button, 2, toggle_string);
}

static gboolean
_init_windows (Example *self)
{
  G3kContext    *g3k = xrd_shell_get_g3k (self->shell);
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);

  VkExtent2D      canvas_extent = {512, 512};
  graphene_size_t canvas_size_meters = {2.0f, 2.0f};
  float           distance = -3.f;

  self->canvas_pixbuf = _create_draw_pixbuf (canvas_extent);

  self->canvas_texture
    = gulkan_texture_new_from_pixbuf (gulkan, self->canvas_pixbuf,
                                      VK_FORMAT_R8G8B8A8_UNORM,
                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                      FALSE);

  self->canvas = xrd_window_new (xrd_shell_get_g3k (self->shell), "Canvas",
                                 NULL, canvas_extent, &canvas_size_meters);
  xrd_shell_add_window (self->shell, self->canvas, NULL, FALSE, NULL);
  g3k_plane_set_texture (G3K_PLANE (self->canvas), self->canvas_texture);

  graphene_point3d_t canvas_point = {
    .x = 0,
    .y = 1.6f - canvas_size_meters.height / 2.f,
    .z = distance,
  };
  graphene_matrix_t transform;
  graphene_matrix_init_translate (&transform, &canvas_point);
  g3k_object_set_matrix (G3K_OBJECT (self->canvas), &transform);

  graphene_size_t size_meters = {0.5f, 0.5f};

  gchar *help_string[] = {"Press A or B below", "to visualize shaking"};
  self->help_button = g3k_button_new (g3k, &size_meters);
  if (!self->help_button)
    return FALSE;
  g3k_button_set_text (self->help_button, 2, help_string);
  graphene_point3d_t help_point = {
    .x = canvas_point.x,
    .y = canvas_point.y + canvas_size_meters.height / 2.f
         + size_meters.height / 2.f,
    .z = distance,
  };
  xrd_shell_add_button (self->shell, self->help_button, &help_point,
                        (GCallback) NULL, self, NULL);

  gboolean compensation = _get_compensation ();
  gchar   *toggle_string[] = {"Comp.", compensation ? "ON" : "OFF"};
  self->toggle_button = g3k_button_new (g3k, &size_meters);
  if (!self->toggle_button)
    return FALSE;
  g3k_button_set_text (self->toggle_button, 2, toggle_string);
  graphene_point3d_t toggle_point = {
    .x = canvas_point.x + canvas_size_meters.height / 2.f
         + size_meters.width / 2.f,
    .y = canvas_point.y,
    .z = distance,
  };
  xrd_shell_add_button (self->shell, self->toggle_button, &toggle_point,
                        (GCallback) _toggle_press_cb, self, NULL);

  gchar *result_string[] = {"Shake:", "0 Pix"};
  self->result_button = g3k_button_new (g3k, &size_meters);
  if (!self->result_button)
    return FALSE;
  g3k_button_set_text (self->result_button, 2, result_string);
  graphene_point3d_t result_point = {
    .x = canvas_point.x + canvas_size_meters.width / 2.f
         + size_meters.width / 2.f,
    .y = toggle_point.y - size_meters.height / 2.f - size_meters.height / 2.f,
    .z = distance,
  };
  xrd_shell_add_button (self->shell, self->result_button, &result_point,
                        (GCallback) _toggle_press_cb, self, NULL);

  GulkanTexture *texture
    = gulkan_texture_new_from_resource (gulkan, "/res/cursor.png",
                                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                        TRUE);

  G3kCursor *cursor = xrd_shell_get_desktop_cursor (self->shell);
  g3k_cursor_set_hotspot (cursor, 3, 3);
  g3k_plane_set_texture (G3K_PLANE (cursor), texture);

  return TRUE;
}

static void
_cleanup (Example *self)
{
  self->shutdown = TRUE;

  g_signal_handler_disconnect (self->shell, self->click_source);
  g_signal_handler_disconnect (self->shell, self->move_source);
  self->click_source = 0;
  self->move_source = 0;

  /* wm controls and frees these
  g_object_unref (self->help_button);
  g_object_unref (self->canvas_pixbuf);
  g_object_unref (self->result_button);
  g_object_unref (self->canvas);
  g_object_unref (self->toggle_button);
  */

  g_object_unref (self->shell);
  self->shell = NULL;
  g_print ("Cleaned up!\n");
}

static void
_click_cb (XrdShell *shell, XrdClickEvent *event, Example *self)
{
  (void) shell;
  (void) self;
  g_print ("%s: %d at %f, %f\n", event->state ? "click" : "release",
           event->button, (double) event->position->x,
           (double) event->position->y);

  self->pressed_button = event->state ? event->button : 0;

  if (self->pressed_button == 0)
    {
      char res[50];
      snprintf (res, 50, "%.1f Pix", (double) self->max_shake);
      gchar *result_string[] = {"Shake:", res};
      g3k_button_set_text (self->result_button, 2, result_string);
      return;
    }
  else
    {
      graphene_point_init_from_point (&self->start, event->position);
      self->max_shake = 0;
    }

  ColorRGBA color = {.r = 0, .g = 0, .b = 0, .a = 255};
  _draw_at_2d_position (self, event->position, &color, 5);
}

static void
_move_cursor_cb (XrdShell *shell, XrdMoveCursorEvent *event, Example *self)
{
  (void) shell;
  (void) self;
  (void) event;
  /*
  g_print ("move: %f, %f\n",
           event->position->x, event->position->y);
   */
  if (self->pressed_button == 0)
    return;

  ColorRGBA color = {.r = 0, .g = 0, .b = 0, .a = 255};
  _draw_at_2d_position (self, event->position, &color, 5);

  float dist = graphene_point_distance (&self->start, event->position, 0, 0);
  self->max_shake = fmaxf (self->max_shake, dist);
}

static void
_shutdown_cb (GxrContext *context, Example *self)
{
  (void) context;
  g_main_loop_quit (self->loop);
}

int
main ()
{
  Example self = {
    .loop = g_main_loop_new (NULL, FALSE),
    .shell = xrd_shell_new (),
    .shutdown = FALSE,
    .pressed_button = 0,
  };

  _init_windows (&self);

  self.click_source = g_signal_connect (self.shell, "click-event",
                                        (GCallback) _click_cb, &self);
  self.move_source = g_signal_connect (self.shell, "move-cursor-event",
                                       (GCallback) _move_cursor_cb, &self);
  G3kContext *g3k = xrd_shell_get_g3k (self.shell);
  g_signal_connect (g3k, "shutdown", G_CALLBACK (_shutdown_cb), &self);

  g_unix_signal_add (SIGINT, _sigint_cb, &self);

  /* start glib main loop */
  g_main_loop_run (self.loop);

  _cleanup (&self);
  g_main_loop_unref (self.loop);
}
